let shopItemsData = [{
    id:'item1',
    title:'Casual Shirt',
    price:'50',
    description:'Designed with lightweight, breathable fabrics like cotton or linen, often featuring simple patterns or solid colors.',
    img:"./images/men-shirt.png"
},{
    id:'item2',
    title:"Trending Kurti",
    price:'90',
    description:'Embrace elegance and grace with our trending kurtis, the epitome of sophistication and charm with reasonable price.',
    img:'images/kurtihaul.png'
},{
    id:'item3',
    title:"Copper-Threaded Saree",
    price:'190',
    description:' Copper accents woven intricately into the fabric. Perfect for any special occasion, brings the entire attention towards you',
    img:'images/saree-haul.png'
},{
    id:'item4',
    title:"Regular Wear Jeans",
    price:'100',
    description:' Designed for comfort and versatility.Crafted with quality denim and provide a flattering fit while ensuring durability for daily wear',
    img:'images/jeanshaul.png'
}]