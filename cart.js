let basket=JSON.parse(localStorage.getItem("data")) || []
let calculation = () => {
    let cartIcon = document.getElementById("cartAmount")
    let cartValue = (basket.map(x => x.itemCount).reduce((x,y) => x+y,0))
    cartIcon.innerHTML =cartValue;
 }
 
 calculation()



 let label =document.getElementById('label')
 let shoppingCart = document.getElementById('shopping-cart')

 let generateCartItems = () => {
    if(basket.length !==0){
     return (shoppingCart.innerHTML=basket.map((x)=>{
        let  {id,itemCount}=x
        let search = shopItemsData.find((y)=>y.id === id) || []
        return(`
       <div class="cart-item"> 
        <img width=100 src=${search.img}>

         <div class="detailss">

           <div class="title-price-x">
              <h4> 
              <p>${search.title}</p>
              <p class="orginalPrice"> $${search.price}</p>
              </h4>

              <i class="bi bi-x-lg" onclick="removeItem(${id})"></i>

           </div>

            <div class="buttonsGrp">
              <i class="bi bi-dash-lg" onclick = "decrement(${id})"></i>

               <div class="quantity"id=${id}> ${itemCount} </div>

                 <i class="bi bi-plus-lg" onclick = "increment(${id})"></i>
               </div>
            
              <h3>Total Cost: $ ${itemCount * search.price} </h3>
            </div>
         </div>`)
     }).join(''))
    }
    else {

        shoppingCart.innerHTML =``
        label.innerHTML =`
        <h2> Your Cart is Empty</h2>
        <a href="index.html">
        <button> Back to Home </button>
        </a>
        `
    }
 }

 generateCartItems();
 let increment = (id)=>{ // *! here id is the argument which takes the div having class="quantity".
   let selectedItem = id; 
   let search = basket.find((item) => item.id === selectedItem.id) //*! each of the item inside the array "basket", will pass as the argument, here it is denoted as  "item"
   if(search=== undefined){
     basket.push({
         id:selectedItem.id, //*! selectedItem.id is the id of the div that we got from the argument id
         itemCount:1
     })
   }
 
   else {
     search.itemCount++;
   }
   

   
   update(selectedItem.id)
   generateCartItems()
   TotalAmount()
   localStorage.setItem("data",JSON.stringify(basket))
 }
 
 let decrement = (id)=>{
     let selectedItem = id; 
     let search = basket.find((item) => item.id === selectedItem.id) //*! each of the item inside the array "basket", will pass as the argument, here it is denoted as  "item"
     if (search === undefined) {return
     }
     else if(search.itemCount === 0){
      return
     }
   
     else {
       search.itemCount--;
     }
 
     console.log(basket)
     update(selectedItem.id)
     basket = basket.filter((x) => x.itemCount !==0)
     generateCartItems()
     TotalAmount()
     
     localStorage.setItem("data",JSON.stringify(basket))
 }

 let update = (id) => {
   let search = basket.find( (x) => (x.id) === (id))
   document.getElementById(id).innerHTML = search.itemCount
   calculation()
}

let removeItem =(x)=> {
   basket = basket.filter((y)=> x.id !==y.id) 
   calculation()
   generateCartItems()
   localStorage.setItem('data', JSON.stringify(basket));
   TotalAmount()
   
  
   

 }

 let TotalAmount = () =>{
  if (basket.length!==0) {
     let amount = basket.map((x)=> {
       let {itemCount,id} =x;
       let search = shopItemsData.find((y)=> y.id ===id) || []
       return (itemCount * search.price)
     }).reduce((x,y) => x+y ,0)
    label.innerHTML =`
    <h2> Total Bill : $ ${amount}</h2>`
    //  console.log(amount)
  }
  
  else return
 }

 TotalAmount()

