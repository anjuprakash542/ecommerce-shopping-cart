let shop = document.getElementById('shop')

let basket=JSON.parse(localStorage.getItem("data")) || []
let generateShop= ()=> {
    return (shop.innerHTML= shopItemsData.map((x)=> {
        let {id,img,title,description,price} =x;
        let search =basket.find((x) => x.id === id) || []
        
     return ` <div class="item" id="product-id-${id}">
     <img src= "${img}" alt="">
     <div class="details">
         <h3> ${title}</h3>
         <p>${description}.</p>
 
         <div class="price-quantity">
             <h2> $ ${price} </h2>
 
             <div class="buttons">
                 <i class="bi bi-dash-circle-fill" onclick = "decrement(${id})"></i>
                 <div class="quantity"id=${id}> ${search.itemCount=== undefined ? 0:search.itemCount}</div>
                 <i class="bi bi-plus-square-fill" onclick = "increment(${id})"></i>
             </div>
         </div>
     </div>
 </div>`
 

    }).join(""))
}

generateShop();





let increment = (id)=>{ // *! here id is the argument which takes the div having class="quantity".
  let selectedItem = id; 
  let search = basket.find((item) => item.id === selectedItem.id) //*! each of the item inside the array "basket", will pass as the argument, here it is denoted as  "item"
  if(search=== undefined){
    basket.push({
        id:selectedItem.id, //*! selectedItem.id is the id of the div that we got from the argument id
        itemCount:1
    })
  }

  else {
    search.itemCount++;
  }
  

  console.log(basket)
  
  update(selectedItem.id)
  localStorage.setItem("data",JSON.stringify(basket))
}

let decrement = (id)=>{
    let selectedItem = id; 
    let search = basket.find((item) => item.id === selectedItem.id) //*! each of the item inside the array "basket", will pass as the argument, here it is denoted as  "item"
    if (search === undefined) {return
    }
    else if(search.itemCount === 0){
     return
    }
  
    else {
      search.itemCount--;
    }

    console.log(basket)
    update(selectedItem.id)
    basket = basket.filter((x) => x.itemCount !==0)
    
    localStorage.setItem("data",JSON.stringify(basket))
}
let update = (id) => {
    let search = basket.find( (x) => (x.id) === (id))
    document.getElementById(id).innerHTML = search.itemCount
    calculation()
}
 let calculation = () => {
    let cartIcon = document.getElementById("cartAmount")
    let cartValue = (basket.map(x => x.itemCount).reduce((x,y) => x+y,0))
    cartIcon.innerHTML =cartValue;
 }
 
 calculation()